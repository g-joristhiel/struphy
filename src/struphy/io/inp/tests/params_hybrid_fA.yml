grid :
    Nel      : [4, 4, 32] # number of grid cells, >p
    p        : [2, 2, 2]  # spline degree, 
    spl_kind : [True, True, True] # spline type: True=periodic, False=clamped
    bc       : [[null, null], [null, null], [null, null]] # boundary conditions for N-splines (homogeneous Dirichlet='d')
    dims_mask : [True, True, True] # True if the dimension is to be used in the mpi domain decomposition (=default for each dimension).
    nq_el    : [5, 4, 4] # quadrature points per grid cell
    nq_pr    : [4, 4, 4] # quadrature points per histopolation cell (for commuting projectors)
    polar_ck : -1 # C^k smoothness at polar singularity at eta_1=0 (default: -1 --> standard tensor product, 1 : polar splines)

units :
    x  : 1. # length scale unit in m
    B  : 1. # magnetic field unit in T
    n  : 0.0005185219355 # bulk number density unit in 1 x 10^20 m^(-3)

time : 
    dt         : 0.01 # time step
    Tend       : 0.05 # simulation time interval is [0, Tend]
    split_algo : LieTrotter # LieTrotter | Strang

geometry : 
    type : Cuboid # mapping F (possible types seen below)  
    Cuboid : 
        l1 : 0. # start of interval in eta1
        r1 : 1. # end of interval in eta1, r1>l1
        l2 : 0. # start of interval in eta2
        r2 : 1. # end of interval in eta2, r2>l2
        l3 : 0. # start of interval in eta3
        r3 : 7.85398163397 # end of interval in eta3, r3>l3

mhd_equilibrium : 
    type : HomogenSlab # (possible choices seen below)
    HomogenSlab :
        B0x  : 0. # magnetic field in x
        B0y  : 0. # magnetic field in y
        B0z  : 1. # magnetic field in z
        beta : 100. # plasma beta = 2*p*mu_0/B^2
        n0   : 1. # number density

em_fields :
    init :
        type : noise # initial conditions (possible types seen below)
        noise :
            comps :
                a1 : [True, True, True]  # components to be initialized (for scalar fields: no list)
            variation_in : e1e2e3 # noise variation (logical space): e1, e2, e3 (1d), e1e2, e1e3, e2e3 (2d), e1e2e3 (3d)
            amp : 0.   # noise amplitude
        ModesSin : 
            coords : 'logical' # in which coordinates (logical or physical)
            comps :
                a1 : [True, True, True]  # components to be initialized (for scalar fields: no list)
            ls : [0] # Integer mode numbers in x or eta_1 (depending on coords)
            ms : [0] # Integer mode numbers in y or eta_2 (depending on coords)
            ns : [1] # Integer mode numbers in z or eta_3 (depending on coords)
            amps : [0.001] # amplitudes of each mode
        ModesCos :
            coords : 'logical' # in which coordinates (logical or physical)
            comps :
                a1 : [True, True, True]  # components to be initialized (for scalar fields: no list)
            ls : [0] # Integer mode numbers in x or eta_1 (depending on coords)
            ms : [0] # Integer mode numbers in y or eta_2 (depending on coords)
            ns : [1] # Integer mode numbers in z or eta_3 (depending on coords)
            amps : [0.001] # amplitudes of each mode
        TorusModesSin :
            coords : 'logical' # in which coordinates (logical or physical)
            comps :
                a1 : [True, True, True]  # components to be initialized (for scalar fields: no list)
            ms : [1] # poloidal mode numbers
            ns : [0] # toroidal mode numbers
            amps : [0.001] # amplitudes of each mode
            pfuns : ['sin'] # profile function in eta1-direction ('sin' or 'exp')
            pfun_params : [null] # Provides [r_0, sigma] parameters for each "exp" profile fucntion, and null for "sin"
        InitialMHDSlab :
            a  : 1. # minor radius (Lx=a, Ly=2*pi*a)
            R0 : 3. # major radius (Lz=2*pi*R0)
            m  : 0  # poloidal (y) mode number
            n  : 1  # toroidal (z) mode number
            U  : 0.1 # amplitude of Ux/Uy
            A  : 0. # amplitude of Uz
        InitialMHDAxisymHdivEigFun :
            spec : '/path_to_spec/spec.npy' # relative path (to <install_path/struphy>) of the .npy spectrum
            eig_freq_upper : 0.15 # upper search limit of squared eigenfrequency to identify eigenfunction
            eig_freq_lower : 0.14 # lower search limit of squared eigenfrequency to identify eigenfunction
            kind : r # real (r) or imaginary (i) part of eigenfunction
            scaling : 1. # scaling factor to scale the amplitude of the eigenfunction

kinetic :
    electrons :
        temperature : 1.0
    ions :
        ionsshape :
            degree : [2, 2, 2]
            size   : [0.25, 0.25, 0.03125]
        phys_params:
            A : 1 # mass number in units of proton mass
            Z : 1 # signed charge number in units of elementary charge
        markers :
            type        : full_f # full_f or delta_f
            ppc         : 20  # particles per 3d grid cell
            eps         : .25 # MPI send/receive buffer (0.1 <= eps <= 1.0)
            bc_type     : [periodic, periodic, periodic] # remove, reflect or periodic
            loading :
                type          : pseudo_random # particle loading mechanism 
                seed          : 1234 # seed for random number generator
                dir_particles : path_to_particles # directory of particles if loaded externally
                moments       : [0., 0., 0., 1., 1., 1.] # moments of Gaussian s3, see background/moms_spec
                spatial       : uniform # uniform or disc
        init :
            type : Maxwellian6DUniform
            Maxwellian6DUniform :
                n  : 1.
                u1 : 0.
                u2 : 0.
                u3 : 0.
                vth1 : 1.
                vth2 : 1.
                vth3 : 1.
        background :
              type        : 0 # 0: Maxwellian in 6d
              moms_spec   : [0, 0, 0, 0, 0, 0, 0] # Specifier for the moments n0, u0x, u0y, u0z, vth0x, etc. 0 means constant
              moms_params : [1., 0., 0., 2.5, 1., 1., 1.] # parameters needed to specify the moments
        perturbations : # always in logical position space
            type : null # null or possible types seen below
            moms : [n, u3] # which velocity moment to perturb: n, u1, u2, u3, vth1, vth2, vth3
            Modes_sin : 
                k1 : [6.283186] # wave vectors in eta_1 
                k2 : [0.] # wave vectors in eta_2 
                k3 : [0.] # wave vectors in eta_3 
                amp : [0.001] # amplitudes
            Modes_cos : 
                k1 : [6.283186] # wave vectors in eta_1 
                k2 : [0.] # wave vectors in eta_2
                k3 : [0.] # wave vectors in eta_3
                amp : [0.001] # amplitudes
        save_data :
            n_markers : 3 # number of markers to be save during simulation
            f :
                slices : [v3]
                n_bins : [[32]]
                ranges : [[[-3., 3.]]]
        push_algos :
            pxb : analytic # possible choices: analytic, implicit
            eta : rk4 # possible choices: forward_euler, heun2, rk2, heun3, rk4
        pc : full # for pressure coupling: full or perp

solvers :
    solver_1 : 
        type : PConjugateGradient
        pc : MassMatrixPreconditioner # null or fft
        tol : 1.e-12
        maxiter : 3000
        info : False
        verbose : False
    solver_2 :
        type : PConjugateGradient
        pc : MassMatrixPreconditioner # null or fft
        tol : 1.e-12
        maxiter : 3000
        info : False
        verbose : False
    solver_3 :
        type : PConjugateGradient
        pc : MassMatrixPreconditioner # null or fft
        tol : 1.e-12
        maxiter : 3000
        info : False
        verbose : False
