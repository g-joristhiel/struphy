.. _kinetic_backgrounds:

Kinetic initial conditions/backgrounds
======================================

Documented modules:

.. currentmodule:: ''

.. autosummary::
    :nosignatures:
    :toctree: STUBDIR

    struphy.kinetic_background.base
    struphy.kinetic_background.maxwellians

.. toctree::
    :caption: Lists of available kinetic initial conditions/backgrounds:

    STUBDIR/struphy.kinetic_background.base
    STUBDIR/struphy.kinetic_background.maxwellians


Base classes
------------

.. automodule:: struphy.kinetic_background.base
    :members:
    :undoc-members: 
    :show-inheritance:


Available Maxwellians
---------------------

.. automodule:: struphy.kinetic_background.maxwellians
    :members:
    :undoc-members: 
    :show-inheritance:

