Struphy documentation
=====================

**Struphy (STRUcture-Preserving HYbrid codes) is a plasma physics library for solving partial differential equations based
on finite element and particle-in-cell methods.**

Struphy is **open source** (https://pypi.org/project/struphy/) and can be :ref:`installed on any architecture <install>`.

Struphy is **open development** and can be improved by you. The code is entirely
written in **Python 3**. The repository lies on the Gitlab instance of Max Planck Computing and Data Facility (MPCDF) 
(https://gitlab.mpcdf.mpg.de/struphy/struphy).

To add code, you need an MPCDF Gitlab account. In case you are not affiliated with the Max Planck Society, 
please :ref:`contact` a Max Planck employee for an invitation.

For further information on **forking** and **merge- (or pull-) requests** please go to :ref:`git_workflow`.

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   sections/overview
   sections/install
   sections/quickstart
   sections/discretization
   sections/notebooks
   sections/userguide
   sections/developers
   sections/models
   sections/propagators
   sections/accumulators
   sections/domains
   sections/mhd_equils
   sections/kinetic_backgrounds
   sections/inits
   sections/diagnostics
   sections/dispersions
   sections/examples

.. _contact:

Contact
-------

Struphy is constantly maintained. Please contact 

   * stefan.possanner@ipp.mpg.de
   * eric.sonnendruecker@ipp.mpg.de
   * xin.wang@ipp.mpg.de

for questions.




   


