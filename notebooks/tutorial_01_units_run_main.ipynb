{
 "cells": [
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Run Struphy main file in notebook\n",
    "\n",
    "In this tutorial we will learn about the Struphy main execution file `struphy/models/main/main.py`. This file is executed upon calling\n",
    "```\n",
    "    $ struphy run MODEL\n",
    "```\n",
    "from the console. We will\n",
    "\n",
    "1. Import `struphy/models/main/main.py` and look at its functionality.\n",
    "2. Import the parameters file `params_mhd_vlasov.yml` and change some parameters.\n",
    "3. Understand the normalization of Struphy models (which units are used).\n",
    "3. Run the model `LinearMHDVlasovCC` in the notebook (without invoking the console).\n",
    "\n",
    "Let us start with step 1."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from struphy.models.main import main\n",
    "\n",
    "main?"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The function `main.py` has the three mandatory arguments `model_name`, `parameters` and `path_out`. In this example, we shall simulate the current coupling hybrid model `LinearMHDVlasovCC`. The simulation results will be stored in the curnt working directory under the folder `struphy_run_main/`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import os\n",
    "\n",
    "model_name = 'LinearMHDVlasovCC'\n",
    "\n",
    "path_out = os.path.join(os.getcwd(), 'struphy_run_main/')"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As a next step, we want to inspect available parameters files for our run. Template parameter files for each model are available in the struphy installation path (`struphy -p` in the console) un der the folder `io/inp/`. Let us check these out:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import struphy\n",
    "\n",
    "inp_path = os.path.join(struphy.__path__[0], 'io/inp')\n",
    "\n",
    "os.listdir(inp_path)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The file `params_mhd_vlasov.yml` is the one for our run. Let us import it with the `yaml` package and print the obtained dictionary:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "params_path = os.path.join(inp_path, 'params_mhd_vlasov.yml')\n",
    "\n",
    "import yaml\n",
    "\n",
    "with open(params_path) as file:\n",
    "    parameters = yaml.load(file, Loader=yaml.FullLoader)\n",
    "    \n",
    "parameters"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "First, let us understand the units used in Struphy (model normalization). In the present example, the geometry is a `Cuboid` with specific left and right boundaries (and thus side length) in each of the three space directions:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "parameters['geometry']"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The question arises in which units of length these numbers are expressed. From the console, the units could be checked by typing\n",
    "```\n",
    "    $ struphy units -i params_mhd_vlasov.yml LinearMHDVlasovCC\n",
    "```\n",
    "Here, you have to pass two informations, namely the parameter file and the model name. The latter is obvious because each Struphy model has its own specific normalization, stated in the model's documentation (and docstring). However, the units of each model can also be influenced by the user via the parameter file. Let us check the relevant section in the dictionary:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "parameters['units']"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Indeed, the user can set\n",
    "\n",
    "1. the unit of length $\\hat x$ in meter\n",
    "2. the unit of the magnetic field strength $\\hat B$ in Tesla\n",
    "3. the unit of the number density $\\hat n$ in $10^{20}$ $m^{-3}$.\n",
    "\n",
    "In the above example we have $\\hat x \\approx 0.023\\,m$, $\\hat B = 1\\,T$ and $\\hat n = 10^{20}$ $m^{-3}$. "
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    " All other units, such as for velocity $\\hat v$ or time $\\hat t$ etc., are derived from the specific normalization of the model. In particular, each model has built-in one of the following three choices for the velocity unit $\\hat v$:\n",
    "\n",
    "1. speed of light, $\\hat v = c$\n",
    "2. Alfvén speed of the bulk species, $\\hat v = v_\\textnormal{A, bulk} = \\sqrt{\\hat B^2 / (m_\\textnormal{bulk} \\hat n \\mu_0)}$\n",
    "3. Cyclotron speed of the bulk species, $\\hat v = \\hat x \\Omega_\\textnormal{c, bulk}/(2\\pi) = \\hat x\\, q_\\textnormal{bulk} \\hat B /(m_\\textnormal{bulk}2\\pi)$\n",
    "\n",
    "We can see that each of these velocity units can be defined in terms of the three units $\\hat x$, $\\hat B$ and $\\hat n$ provided by the user. The associated time scale is then automatically given by\n",
    "$$\n",
    " \\hat t = \\hat x / \\hat v \\,.\n",
    "$$\n",
    "\n",
    " The velocity unit (in one-to-one correspondence with the `timescale`) and the `bulk_species` are fixed within each model, for instance:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from struphy.models.hybrid import LinearMHDVlasovCC\n",
    "\n",
    "print(LinearMHDVlasovCC.timescale())\n",
    "print(LinearMHDVlasovCC.bulk_species())"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The charge number `Z` and the mass number `A` of the bulk species can be set by the user via the parameter file:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "parameters['fluid'][LinearMHDVlasovCC.bulk_species()]['phys_params']"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Please check out https://struphy.pages.mpcdf.de/struphy/sections/models.html#normalization for further discussion on the units used in Struphy. In this tutorial, instead of the console, we can inspect the units of our run also directly in this notebook:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "units, eq_params = LinearMHDVlasovCC.model_units(parameters, verbose=True)\n",
    "units"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "eq_params"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The side lengths of the `Cuboid` in our example are:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print('s1 = ', (parameters['geometry']['Cuboid']['r1'] - parameters['geometry']['Cuboid']['l1']) * units['x'], 'm')\n",
    "print('s2 = ', (parameters['geometry']['Cuboid']['r2'] - parameters['geometry']['Cuboid']['l2']) * units['x'], 'm')\n",
    "print('s3 = ', (parameters['geometry']['Cuboid']['r3'] - parameters['geometry']['Cuboid']['l3']) * units['x'], 'm')"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us get back to the parameter file and change some entries in the parameter dictionary before we run the model.\n",
    "\n",
    "The end time of 50 is too long for our example and we wish to simulate more particles-per-cell than 200 to have a higher resolution. Let us change these two parameters and show the whole dictionary again."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "parameters['time']['Tend'] = 1.5\n",
    "\n",
    "parameters['kinetic']['energetic_ions']['markers']['ppc'] = 400\n",
    "\n",
    "parameters"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We are now ready to call the Struphy main file. A tutorial of how to post process the generated simualtion data is available in the notebook `diagnostics_1_standard_plots`. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "main(model_name, parameters, path_out)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "env",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.6"
  },
  "orig_nbformat": 4
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
